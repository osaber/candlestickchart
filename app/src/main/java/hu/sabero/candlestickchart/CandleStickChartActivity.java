package hu.sabero.candlestickchart;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.data.CandleDataSet;
import com.github.mikephil.charting.data.CandleEntry;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This Class represents a candelstick chart view
 * Created by Saber ouechtati on 2016. 07. 17..
 */
public class CandleStickChartActivity extends FragmentActivity {

    @BindView(R.id.button1)
    Button button1;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;
    @BindView(R.id.button4)
    Button button4;
    @BindView(R.id.button5)
    Button button5;
    @BindView(R.id.chartView)
    CandleStickChart chartView;

    private ArrayList<CandleEntry> yValueList;
    private Button selectedButton;
    private XAxis xAxis;
    private YAxis yAxis;
    private int xMaxValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_candlechart);
        ButterKnife.bind(this);

        initialize();
        showChart();

        setOnTouchListener(button1, 60);
        setOnTouchListener(button2, 70);
        setOnTouchListener(button3, 120);
        setOnTouchListener(button4, 180);
        setOnTouchListener(button5, 220);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.candle, menu);
        return true;
    }

    private void setOnTouchListener(final Button button, final int xMaxValue) {
        button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setSelectedButton(button);
                setxMaxValue(xMaxValue);
                showChart();
                chartView.animateX(3000);
                return false;
            }
        });
    }

    /**
     * This method intitialize the chart view
     */
    private void initialize(){

        setSelectedButton(button1);
        chartView.setBackgroundColor(Color.WHITE);
        chartView.setDescription("Saber Ouechtati, Candlestick example (y=9*cos(x)+20)");
        chartView.setMaxVisibleValueCount(50);

        // scaling can now only be done on x- and y-axis separately
        chartView.setPinchZoom(false);
        chartView.setDrawGridBackground(false);
        xAxis = chartView.getXAxis();
        xAxis.setPosition(XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(true);

        YAxis leftAxis = chartView.getAxisLeft();
        leftAxis.setEnabled(false);

        yAxis = chartView.getAxisRight();
        yAxis.setEnabled(true);
        yAxis.setLabelCount(7, false);
        yAxis.setDrawGridLines(false);
        yAxis.setDrawAxisLine(false);

        chartView.getLegend().setEnabled(false);
        setxMaxValue(60);
    }

    /**
     * This method modify the  background color and the text color of the selected button object
     * @param button: the new selected button object
     */
    private void setSelectedButton(Button button) {
        if (selectedButton != null) {
            selectedButton.setBackgroundColor(Color.GRAY);
            selectedButton.setTextColor(Color.BLACK);
        }
        selectedButton = button;
        selectedButton.setBackgroundColor(Color.BLACK);
        selectedButton.setTextColor(Color.WHITE);
    }

    /**
     * This method add the preprepared chart data to the chart view and display it
     */
    private void showChart() {

        chartView.resetTracking();
        prepareChartData();
        CandleDataSet set1 = new CandleDataSet(yValueList, "Data Set");
        set1.setAxisDependency(AxisDependency.LEFT);
        set1.setShadowColor(Color.DKGRAY);
        set1.setShadowWidth(0.7f);

        set1.setDecreasingColor(Color.RED);
        set1.setDecreasingPaintStyle(Paint.Style.FILL);
        set1.setIncreasingColor(Color.rgb(122, 242, 84));
        set1.setIncreasingPaintStyle(Paint.Style.FILL_AND_STROKE);
        set1.setNeutralColor(Color.BLUE);

        CandleData data = new CandleData(set1);

        chartView.setData(data);
        chartView.invalidate();
    }

    /**
     * This method generate chart data and add it to yValueList object
     * function: y = 9 cos(x) + 20
     */
    private void prepareChartData() {

        yValueList = new ArrayList<CandleEntry>();

        float oldValue = 0;
        for (int i = 0; i < getxMaxValue(); i++) {
            float value = (float) (9 * Math.cos(i) + 20);
            float high = (float) (Math.random() * 9) + 8f;
            float low = (float) (Math.random() * 9) + 8f;
            float open = (float) (Math.random() * 6) + 1f;
            float close = (float) (Math.random() * 6) + 1f;

            boolean even = false;
            if (value < oldValue) {
                even = true;
            }

            oldValue = value;
            yValueList.add(new CandleEntry(i, value + high, value - low, even ? value + open : value - open,
                    even ? value - close : value + close));
        }
    }

    /**
     * This method request the maximum value of xAxis
     * @return int: xMaxValue
     */
    public int getxMaxValue() {
        return xMaxValue;
    }

    /**
     * This method modify the maximum value of xAxis
     * @param xMaxValue
     */
    public void setxMaxValue(int xMaxValue) {
        this.xMaxValue = xMaxValue;
    }
}
